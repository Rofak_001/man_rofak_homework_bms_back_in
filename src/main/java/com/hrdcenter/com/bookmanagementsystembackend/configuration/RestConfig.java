package com.hrdcenter.com.bookmanagementsystembackend.configuration;

import com.hrdcenter.com.bookmanagementsystembackend.model.Book;
import com.hrdcenter.com.bookmanagementsystembackend.model.Category;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class RestConfig implements RepositoryRestConfigurer {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Book.class, Category.class);
    }
}
