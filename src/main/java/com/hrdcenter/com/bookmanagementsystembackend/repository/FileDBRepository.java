package com.hrdcenter.com.bookmanagementsystembackend.repository;

import com.hrdcenter.com.bookmanagementsystembackend.model.FileDB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileDBRepository extends JpaRepository<FileDB, String> {
}
