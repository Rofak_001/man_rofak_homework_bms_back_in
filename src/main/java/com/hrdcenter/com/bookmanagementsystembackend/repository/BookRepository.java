package com.hrdcenter.com.bookmanagementsystembackend.repository;


import com.hrdcenter.com.bookmanagementsystembackend.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book,Integer> {
    @RestResource(path = "bookTitle",rel = "SearchBookByTitle")
    List<Book> findByTitleIsContainingIgnoreCase(String title);
    @Query(value = "SELECT b FROM tbl_books b  Join Fetch b.category c Where lower(c.title) like lower(concat('%',:title,'%'))")
    @RestResource(path = "categoryTitle",rel = "SearchBookByCategoryTitle")
    List<Book> findByBookByCategoryTitle(@Param("title") String categoryTitle);
}
