package com.hrdcenter.com.bookmanagementsystembackend.repository;

import com.hrdcenter.com.bookmanagementsystembackend.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {
}
