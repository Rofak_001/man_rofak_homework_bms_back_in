package com.hrdcenter.com.bookmanagementsystembackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookManagementSystemBackEndApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookManagementSystemBackEndApplication.class, args);
    }

}
